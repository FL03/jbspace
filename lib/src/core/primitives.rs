/*
    Appellation: primitives <module>
    Contributors: FL03 <jo3mccain@icloud.com> (https://gitlab.com/FL03)
    Description:
        ... Summary ...
*/
#[doc(inline)]
pub use self::{constants::*, types::*};

mod constants {

}

mod types {}
